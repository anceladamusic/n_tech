$('.user_panel_wrapper').delegate('#logout', 'click', function(){
	parameters = {};
	parameters['method'] = 'logout';
	makeAjax(parameters);
});

//если на кнопку авторизироваться
$('#login_button').on('click', function(){
	buildAuthForm($(this));
});

//если нажать на авторизирваться вверху экрана
$('.user_panel_wrapper').delegate('#login_header', 'click', function(){
	buildAuthForm($(this));
});

$('#login').on('click', function(){
	buildAuthForm($(this));
});

function buildAuthForm(block){
	parameters = {};
	parameters['method'] = 'build_auth_form';
	parameters['url'] = block.attr('data-url');
	makeAjax(parameters);
}

$('.user_panel_wrapper').delegate('#registration_request_header', 'click', function(){
	buildRegistrationForm();
	authorizationOpen();
});

$('.authorization_content_inner').delegate('#registration_request', 'click', function(){
	buildRegistrationForm();
});

function buildRegistrationForm(){
	parameters = {};
	parameters['method'] = 'build_registration_form';
	makeAjax(parameters);
}

//кнопка авторизироваться
$('.authorization_content_inner').delegate('.auth_submit_button', 'click', function(){
	authSubmit();
});

function authSubmit(){
	parameters = {};
	parameters['login'] = $('#auth_login').val();
	parameters['password'] = $('#auth_password').val();
	parameters['url'] = $('.auth_submit_button').attr('data-id');
	parameters['method'] = 'try_login';
	makeAjax(parameters);
}

// отправить заявку на регистрацию
$('.authorization_content_inner').delegate('.registration_submit_button', 'click', function(){
	registrationSubmit();
});

function registrationSubmit(){
	parameters = {};
	parameters['name'] = $('#registration_name').val();
	parameters['email'] = $('#registration_email').val();
	parameters['telephone'] = $('#registration_telephone').val();
	parameters['organization'] = $('#registration_organization').val();
	parameters['text'] = $('#registration_text').val();
	parameters['method'] = 'submit_registration';
	if (parameters['name'] == '' && parameters['email'] == '' && parameters['organization'] == ''){
		$('.incorrect span').html('Не все поля формы заполнены')
	} else {
		$('.incorrect span').html('');
		makeAjax(parameters);
	}
}

$(document).keypress(function(e){
	if (e.which == 13){
		if (($('#auth_login').is(':focus')==false) && ($('#auth_password').is(':focus')==false)){
			authSubmit();
		} else if ( ($('#registration_name').is(':focus') == false) && 
		($('#registration_email').is(':focus') == false) && 
		($('#registration_telephone').is(':focus') == false) &&
		($('#registration_organization').is(':focus') == false) &&
		($('#registration_text').is(':focus') == false) ){
			authSubmit();
		}
	}
});

function readResponseAuthorization(data, parameters){
	if (parameters['method'] == 'build_auth_form'){
		$('.authorization_content_inner').html(data['auth_form']);
		authorizationOpen();
	} else if (parameters['method'] == 'logout'){
		$('.user_panel_wrapper').html(data['login_panel_html']);
		window.location = '/';
	} else if (parameters['method'] == 'try_login'){
		if (data['answer'] == false){
			$('.incorrect span').html('Некорректные имя пользователя и/или пароль.')
		} else {
			window.location = parameters['url'];
		}
	} else if (parameters['method'] == 'build_registration_form'){
		$('.authorization_content_inner').html(data['registration_form']);
	} else if (parameters['method'] == 'submit_registration'){
		if (data['status'] == 'success'){
			$('.correct span').html(data['text']);
			$('#registration_name').val('');
			$('#registration_email').val('');
			$('#registration_telephone').val('');
			$('#registration_organization').val('');
			$('#registration_text').val('');
			setTimeout(authorizationClose, 1000);
		} else {
			$('incorrect span').html(data['text'])
		}
	}
}