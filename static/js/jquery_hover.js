hover_event = {

	'mouseover': {
		'block': undefined,
	},

	'mouseleave': {
		'block': undefined,	
	},

}

$('.production_item_content').on('mouseover', function(){
	if ($(this).find($('.lock_active')).length > 0){
		lock_switch('mouseover', $('.lock_active'), $('.lock_passive'));	
	}
});

$('.production_item_content').on('mouseleave', function(){
	if ($(this).find($('.lock_active')).length > 0){
		lock_switch('mouseleave', $('.lock_passive'), $('.lock_active'));
	}
});


function lock_switch(type, active_block, passive_block){
	$.each(hover_event, function(key){
		if (key == type){
			hover_event[key]['block'] = active_block;
			hover_event[key]['block'].show();
		} else {
			hover_event[key]['block'] = passive_block;
			hover_event[key]['block'].hide();
		}
	});
}

console.log($('.production_item_content').find($('.lock_active')));