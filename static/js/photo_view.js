var image_module = {}

//блоки
image_module.image_wrapper = $('#modal_image_wrapper');
image_module.image_inner = $('.modal_image_inner');
//события

$('.photo').on('click', function(){
	photo_id = parseInt($(this).attr('data-id'));
	switchPhoto(photo_id)
});

//закрыть окно
$('.modal_image_inner').delegate('.close_photo', 'click', function(){
	$('#modal_image_wrapper').hide(200);
});

//следующее фото
$('.modal_image_inner').delegate('#next_photo', 'click', function(){
	photo_id = parseInt($(this).attr('data-id'));
	switchPhoto(photo_id)
});

//следующее фото
$('.modal_image_inner').delegate('#previous_photo', 'click', function(){
	photo_id = parseInt($(this).attr('data-id'));
	switchPhoto(photo_id);
});

function switchPhoto(photo_id){
	parameters = {};
	parameters['method'] = 'view_photo';
	parameters['photo_id'] = photo_id;
	makeAjax(parameters);
}

function readResponsePhotoView(data, parameters){
	if (parameters['method'] == 'view_photo'){

		image_module.image_inner.fadeOut(100)
			.delay(200)
			.queue(function(n){
				$(this).html(data['answer']);
				n();
		}).fadeIn(100);

		if (image_module.image_wrapper[0]['style']['display']!='flex'){
			modalImageOpen();
		}

	}
}

//по нажатию на spacebar следующяя картинка
$('body').keydown(function(e){
	if (e.keyCode == 32){
		if ($('#next_photo').length > 0){
			e.preventDefault();
			e.stopPropagation();
			photo_id = $('#next_photo').attr('data-id');
			switchPhoto(photo_id);
		}
	}
});