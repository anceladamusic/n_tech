$('#carousel_items_panel').delegate('.associated_unit_previous', 'click', function(){

	parameters = {};
	parameters['method'] = 'get_carousel_units';
	parameters['associated_parent_id'] = parseInt($(this).attr('data-parent'));
	parameters['first_unit_id'] = parseInt($(this).attr('data-id'));
	parameters['items_count'] = parseInt($(this).attr('data-count'));
	parameters['type'] = 'previous';

	console.log(parameters)

	makeAjax(parameters);

});

$('#carousel_items_panel').delegate('.associated_unit_next', 'click', function(){

	parameters = {};
	parameters['method'] = 'get_carousel_units';
	parameters['associated_parent_id'] = parseInt($(this).attr('data-parent'));
	parameters['first_unit_id'] = parseInt($(this).attr('data-id'));
	parameters['items_count'] = parseInt($(this).attr('data-count'));
	parameters['type'] = 'next';

	console.log(parameters);

	makeAjax(parameters);
});

function readResponseGetCarouselUnits(data, parameters){
	if (parameters['method'] == 'get_carousel_units'){
		$('#carousel_items_panel').html(data['html']);
	}
}