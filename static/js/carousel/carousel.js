var carousel = [];

//нажать на след, предидущ элемент
$('.carousel_change_unit').on('click', function(){
	unit_id = parseInt($(this).attr('data-id'));
	showById(unit_id);
})

$('.carousel_small_circle').on('click', function(){
	carousel_id = parseInt($(this).attr('data-id'));
	showById(carousel_id);
});

function carouselInit(){

	$.each($('.carousel_unit_wrapper'), function(){
		unit_id = parseInt($(this).attr('data-id'));
		carousel.push({'id': unit_id, 'html': $(this), 'selected': 0});
	});

	carousel[0]['html'].show();
	carousel[0]['selected'] = 1;
}

function showSelected(){
	$.each(carousel, function(i){
		if(carousel[i]['selected'] == 1){
			carousel[i]['html'].delay(200).fadeIn(200);
		} else {
			carousel[i]['html'].fadeOut(200);
		}
	});
}

function showById(carousel_id){
	$.each(carousel, function(i){
		if (carousel[i]['id'] == carousel_id){
			carousel[i]['selected'] = 1;
		} else {
			carousel[i]['selected'] = 0;
		}
	});
	showSelected();
}

function showNext(){
	$.each(carousel, function(i){
		if (carousel[i]['selected'] == 1){
			if (i < carousel.length-1){
				carousel[i]['selected'] = 0;
				carousel[i+1]['selected'] = 1;
				showSelected();
				return false;
			} else if (i == carousel.length -1){
				carousel[i]['selected'] = 0;
				carousel[0]['selected'] = 1;
				showSelected();
				return false; 
			}
		}
	});
}


carouselInit();
setInterval(showNext, 10000);