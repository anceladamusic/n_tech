// функция инициализация 
var siteInit = function(){
}

// функции окна авторизации
var authorizationClose = function(){
	$('#authorization_window_wrapper').hide();
}

var authorizationOpen = function(){
	$('#authorization_window_wrapper').show();
}

//функция модального окна картинки
var modalImageClose = function(){
	$('#modal_image_wrapper').hide(200);
}

var modalImageOpen = function(){
	$('#modal_image_wrapper').show(200);
	$('#modal_image_wrapper').css({display:'flex'});
}


// фунции модального окна
var modalClose = function(){
	$('#modal_window_wrapper').hide();
}

var modalOpen = function(){
	$('#modal_window_wrapper').show();
}

//при нажатии esc закрыть модальное окно

// функции окна загрузки
var loadingOpen = function(){
	$('#loading_shadow_wrapper').show();
}

var loadingClose = function(){
	$('#loading_shadow_wrapper').hide();
}

//при переоде по ссылке, показать блок загрузка
$('a').on('click', function(){
	link = $(this).attr('href');
	if (link != '#'){
		loadingOpen();
		setTimeout(loadingClose, 2000);	
	}
});

//скрыть окно авторизации, если клик вне содержания
$('#authorization_window_wrapper').on('click', function(){
	if ($('#authorization_content').is(':hover') == false){
		$(this).fadeOut(200);
	}
});

//скрыть модальное окно, если клик вне содрежания
$('#modal_window_wrapper').on('click', function(){
	if ($('#modal_content').is(':hover') == false){
		$(this).fadeOut(200);
	}
});

//скрыть модальное окно картинок, если клик вне содрежания
$('#modal_image_wrapper').on('click', function(){

	hovered = 0;

	$.each($('.photo_arrow'), function(){
		if ($(this).is(':hover') == true){
			hovered = 1;
			return false;
			
		}
	});

	if ($('.modal_image_inner').children('img').is(':hover') == false){
		if (hovered == 0){
			$('#modal_image_wrapper').hide(200);	
		}
	}
});

//скрыть модальное окно, если клик на закрыть
$('#modal_window_close_button').on('click', function(){
	$('#modal_window_wrapper').fadeOut(200);
});


//скрыть модальное окно, если esc кнопка
$(document).keydown(function(e){
	if (e.keyCode == 27){
		if($('#modal_window_wrapper').is(':visible')){
			$('#modal_window_wrapper').fadeOut(200);
		}else if($('#modal_image_wrapper').is(':visible')){
			$('#modal_image_wrapper').hide(200);
		}
	}
});

siteInit();
loadingOpen();

$('body').waitForImages(function() {
    loadingClose();
});

$('#modal_image_wrapper').on({
	'mousewheel': function(e){
		if (e.target.id == 'el') return;
		e.preventDefault();
		e.stopPropagation();
	}
})

$('#modal_window_wrapper').on({
	'mousewheel': function(e){
		if (e.target.id == 'el') return;
		e.preventDefault();
		e.stopPropagation();
	}
})

$('#authorization_window_wrapper').on({
	'mousewheel': function(e){
		if (e.target.id == 'el') return;
		e.preventDefault();
		e.stopPropagation();
	}	
})