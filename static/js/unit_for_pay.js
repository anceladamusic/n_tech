if (typeof unit != 'undefined'){
	unit['description1_html'] = $('#description1');
	unit['description2_html'] = $('#description2');

	unit['functional_html'] = $('#functional');
	unit['features_html'] = $('#features');
}

$('#description1_button').on('click', function(){
	$(this).addClass('color_red_link');
	$('#description2_button').removeClass('color_red_link');

	$('#functional_button').removeClass('color_red_link');
	$('#features_button').removeClass('color_red_link');

	unit['description1_html'].show();
	unit['description2_html'].hide();

	unit['functional_html'].hide();
	unit['features_html'].hide();
});

$('#description2_button').on('click', function(){
	$(this).addClass('color_red_link');
	$('#description1_button').removeClass('color_red_link');

	$('#functional_button').removeClass('color_red_link');
	$('#features_button').removeClass('color_red_link');

	unit['description1_html'].hide();
	unit['description2_html'].show();

	unit['functional_html'].hide();
	unit['features_html'].hide();
});

$('#functional_button').on('click', function(){
	$(this).addClass('color_red_link');
	$('#features_button').removeClass('color_red_link');

	$('#description1_button').removeClass('color_red_link');
	$('#description2_button').removeClass('color_red_link');

	unit['description1_html'].hide();
	unit['description2_html'].hide();

	unit['functional_html'].show();
	unit['features_html'].hide(); 
});

$('#features_button').on('click', function(){
	$(this).addClass('color_red_link');
	$('#functional_button').removeClass('color_red_link');

	$('#description1_button').removeClass('color_red_link');
	$('#description2_button').removeClass('color_red_link');

	unit['description1_html'].hide();
	unit['description2_html'].hide();

	unit['functional_html'].hide();
	unit['features_html'].show();
});