//изменение числа в карзине
$('.quantity_change').on('click', function(){
	parameters = {};
	unit_id = parseInt($(this).attr('data-id'))
 	parameters['unit_id'] = unit_id;
	parameters['quantity'] = parseInt($('.cart_item_quantity_input[data-id="'+unit_id+'"]').val());
	parameters['method'] = 'quantity_change';
	makeAjax(parameters);
});

//добавить в корзину
$('#add_to_cart').on('click', function(){
	parameters = {};
	parameters['method'] = 'add_to_cart';
	parameters['quantity'] = parseInt($('.cart_item_quantity_input').val());
	parameters['unit_id'] = parseInt($(this).attr('data-id'));
	makeAjax(parameters);
});

//изменяем количество при добавить в корзину
//увеличить
$('.cart_item_quantity_plus').on('click', function(){
	unit_id = parseInt($(this).attr('data-id'));

	current = parseInt($('.cart_item_quantity_input[data-id="'+unit_id+'"]').val());
	current += 1;
	$('.cart_item_quantity_input[data-id="'+unit_id+'"]').val(current);
});

//уменьшить
$('.cart_item_quantity_minus').on('click', function(){
	unit_id = parseInt($(this).attr('data-id'));

	current = parseInt($('.cart_item_quantity_input[data-id="'+unit_id+'"]').val());
	if (current > 0){
		current-=1;	
	}
	$('.cart_item_quantity_input[data-id="'+unit_id+'"]').val(current);
});

$('#submit_order').on('click', function(){
	parameters = {};
	parameters['name'] = $('#orderer_name').val();
	parameters['telephone'] = $('#orderer_telephone').val();
	parameters['email'] = $('#orderer_email').val();
	parameters['text'] = $('#orderer_text').val();
	parameters['method'] = 'submit_cart';
	checkOrdererInfo(parameters);
});


function checkOrdererInfo(parameters){

	$('#message_error').html('');
	$('#message_success').html('');

	if (parameters['name'] == ''){
		$('#message_error').html('Отсутствует имя');
	} else if (parameters['telephone'] == ''){
		$('#message_error').html('Отсутствует телефон');
	} else if (parameters['text'] == ''){
		$('#message_error').html('Отсутствует текст');
	} else {
		$('#message_error').html('');
		makeAjax(parameters);
	}
}

function readResponseCart(data, parameters){
	if (parameters['method'] == 'submit_cart'){
		if (data['status'] == 'success') {
			$('#orderer_name').val('');
			$('#orderer_telephone').val('');
			$('#orderer_email').val('');
			$('#orderer_text').val('');	

			$('#message_success').html('Ваш заказ успешно направлен');

			$('tbody').html('');

			$('.cart_wrapper').html(data['cart_html']);
		} else {
			$('#message_error').html('Отсутствуют товары в корзине');
		}
	} else if (parameters['method'] == 'add_to_cart'){
		$('.cart_wrapper').html(data['cart_html']);
		console.log(data);
	} else if (parameters['method'] == 'quantity_change'){
		$('.cart_wrapper').html(data['cart_html']);
		console.log(data);
	}
}