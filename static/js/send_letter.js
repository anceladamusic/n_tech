$(document).keypress(function(e){
	if (e.which == 13){
		if (($('#letter_name').is(':focus') == false) && ($('#letter_telephone').is(':focus') == false) &&
			($('#letter_email').is(':focus') == false) && ($('#letter_text').is(':focus') == false)){
			processSubmitLetter();
		}
	}
});


$('#submit_letter').on('click', function(){
	processSubmitLetter();
});


function processSubmitLetter(){
	parameters = {};
	parameters['letter_name'] = $('#letter_name').val();
	parameters['letter_telephone'] = $('#letter_telephone').val();
	parameters['letter_email'] = $('#letter_email').val();
	parameters['letter_text'] = $('#letter_text').val();
	parameters['method'] = 'submit_letter';


	if (parameters['letter_name'] == ''){
		$('#submit_letter_message_error').html('Отсутствует Контактное лицо')
	} else if(parameters['letter_telephone'] == ''){
		$('#submit_letter_message_error').html('Отсутствует поле Телефон');
	} else if(parameters['letter_text'] == ''){
		$('#submit_letter_message_error').html('Отсутствует поле Текст');
	} else {
		$('#submit_letter_message_error').html('');
		makeAjax(parameters);
	}
}

function readResponseSendLetter(data, parameters){
	if (parameters['method'] == 'submit_letter'){
		$('#letter_name').val('');
		$('#letter_telephone').val('');
		$('#letter_email').val('');
		$('#letter_text').val('');

		if (data['email_result'] == true){
			$('#submit_letter_message_alert').val('');
			$('#submit_letter_message_success').html('Сообщение успешно отправлено');
		} else {
			$('#submit_letter_message_success').val('');
			$('#submit_letter_message_alert').html('Во время отправки сообщения произошла ошибка');	
		}

		console.log(data);
	}
}