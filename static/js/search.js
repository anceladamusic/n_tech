$(document).keypress(function(e){
	if (e.which == 13){
		if ($('#words').is(':focus')==false){
			processSubmitLetter();
		}
		if ($('.search_panel_input').is(':focus')==true){
			input_value = get_search_value();
			if (input_value != false){
				window.location = '/search/'+input_value+'/';	
			}
		}
	}
});

$('.search_panel_button').on('click', function(){
	input_value = get_search_value();
	if (input_value != false){
		window.location = '/search/'+input_value+'/';
	}
});

$('#process_searching').on('click', function(){
	processSearching();
});

function get_search_value(){
	var result = false;
	$.each($('.search_panel_input'), function(i){
		if ($('.search_panel_input')[i]['value'].length > 0){
			result = $('.search_panel_input')[i]['value'];
			return false;
		}
	});
	return result;
}

function processSearching(){
	parameters = {};
	parameters['words'] = $('#words').val();
	parameters['method'] = 'process_searching';
	makeAjax(parameters);
}

function readResponseSearch(data, parameters){
	if (parameters['method'] == 'process_searching'){
		$('.search_result_wrapper').html(data['answer']);
	}
}