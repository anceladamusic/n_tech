from django.contrib import admin
from mainapp.models import *
from django_mptt_admin.admin import DjangoMpttAdmin

class UnitAdmin(DjangoMpttAdmin):
	pass
admin.site.register(Unit, UnitAdmin)

class UnitPhotoAdmin(DjangoMpttAdmin):
	pass
admin.site.register(UnitPhoto, UnitPhotoAdmin)


class UnitPriorityAdmin(DjangoMpttAdmin):
	pass
admin.site.register(UnitPriority, UnitPriorityAdmin)

class RegistrationOrderAdmin(admin.ModelAdmin):
	pass
admin.site.register(RegistrationOrder, RegistrationOrderAdmin)


class AssociatedChildrenInline(admin.TabularInline):
	model = AssociatedChildren

class AssociatedParentAdmin(admin.ModelAdmin):
	inlines = [
		AssociatedChildrenInline,
	]
admin.site.register(AssociatedParent, AssociatedParentAdmin)

class UnitCarouselChildrenInline(admin.StackedInline):
	model = UnitCarouselChildren

class UnitCarouselParentAdmin(admin.ModelAdmin):
	inlines = [
		UnitCarouselChildrenInline,
	]
admin.site.register(UnitCarouselParent, UnitCarouselParentAdmin)

class UserManipulationAdmin(admin.ModelAdmin):
	pass
admin.site.register(UserManipulation, UserManipulationAdmin)