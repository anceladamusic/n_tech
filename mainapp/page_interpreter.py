import copy
import urllib

from django.template.loader import render_to_string
from .models import *
from django.contrib import auth
from .base import Base
from functools import reduce
from django.db.models import Q
from django.http import Http404

from .ajax import Ajax
from structured.structured import Structured

class PageInterpreter:

	def __init__(self):

		self.carousel_items_count = 3

		self.unit_exclude_ids = [
			164,
			10,
			1
		]

		self.sts_units_count=0

	# страница Контакты
	def build_contacts(self, request):

		self.__write_user_manipulations(request.user.id, 10)

		return render_to_string('contacts.html')

	def build_main(self, request):

		# главная
		unit = Unit.objects.get(id=1)

		self.__write_user_manipulations(request.user.id, unit.id)

		productions = Unit.objects.filter(parent_id=2)

		is_auth = request.user.is_authenticated()

		products_panel = Base().build_products_panel(is_auth)

		return render_to_string(unit.Template, {
			'unit': unit,
			'products_panel': products_panel,
			'is_auth': is_auth,
		})

	def __locked_for_users(self, parents_tree, unit):
		if any(u.id == unit.id for u in parents_tree):
			if unit.LockedForUser == False:
				return False
		return True

	def __check_locked(self, parents_tree, unit, request):
		if any(u.id == unit.id for u in parents_tree):
			is_auth = request.user.is_authenticated()
			if unit.Locked == False:
				is_auth = True
		else:
			is_auth = True
		return is_auth

	def __check_unit_exclude_ids(self, unit):
		if unit.id in self.unit_exclude_ids:
			raise Http404
		else:
			return True

	# содержание priority_unit
	def __build_priority_content_html(self, priority_unit, request):

		# если уровень 0
		if priority_unit.level == 0:
			photo = UnitPhoto.objects.filter(UnitPriority_id=priority_unit.id)

			photos_children = UnitPhoto.objects.filter(parent_id=photo[0].id)

			children = UnitPriority.objects.filter(parent_id=priority_unit.id)

			children_arr = []

			for child in children:
				children_arr.append(child.id)


			childs_of_childs = UnitPriority.objects.filter(parent_id__in=children_arr)
			childs_of_childs_regroup = []
			for child in children:
				group = {'child_id': child.id, 'childs_of_childs': []}
				for child_of_childs in childs_of_childs:
					if child.id == child_of_childs.parent_id:
						group['childs_of_childs'].append(child_of_childs)
				childs_of_childs_regroup.append(group)


			parents_tree = self.build_priority_unit_parents_tree(priority_unit)

			return render_to_string('unit_priority/unit_priority_lvl1.html', {
				'photo': photo[0],
				'photos_children': photos_children,
				'priority_unit': priority_unit,
				'children': children,
				'childs_of_childs': childs_of_childs_regroup,
				'parents_tree': parents_tree,
			})
		# если другой уровень
		else:

			parents_tree = self.build_priority_unit_parents_tree(priority_unit)

			priority_unit_child_production_panel_html =  \
			self.build_priority_unit_child_production_panel_html(priority_unit)

			return render_to_string('unit_priority/unit_priority_lvl_other.html', {
				'priority_unit': priority_unit,
				'parents_tree': parents_tree,
				'priority_unit_child_production_panel_html': priority_unit_child_production_panel_html,
			})

	def build_priority_unit_child_production_panel_html(self, priority_unit):

		child_productions = UnitPriority.objects.filter(parent_id = priority_unit.id)

		priority_child_production_items = []

		counter = 1

		for child in child_productions:

			main_photo = UnitPhoto.objects.filter(UnitPriority_id=child.id)

			if len(main_photo) > 0:
				main_photo = main_photo[0]
				photo_childs = UnitPhoto.objects.filter(parent_id=main_photo.id)
			else:
				photo_childs = []

			properties = UnitPriority.objects.filter(parent_id=child.id)

			priority_child_production_items.append( \
				render_to_string('unit_priority/priority_unit_child_item.html', {
						'item': child,
						'counter': counter,
						'main_photo': main_photo,
						'photo_childs': photo_childs,
						'properties': properties,
					}))
			counter += 1

		return render_to_string('unit_priority/priority_unit_child_panel.html', {
			'priority_child_production_items': priority_child_production_items,
		})

	def __build_json_unit(self, unit):
		json_unit = {}
		if unit.Description2:
			json_unit['description2'] = unit.Description2
		json_unit['description1'] = unit.Description
		return json_unit

	def __build_unit_htmls_for_tree(self, units, html):
		result = []
		for unit in units:
			result.append(render_to_string(html, {
				'unit': unit,
			}))
		return result

	def build_content_html(self, unit, request):

		parents_tree = self.build_parents_tree(unit)

		if self.__check_unit_exclude_ids(unit):

			# level_1 раздел продукция
			if unit.level == 1 and unit.id == 2:

				self.__write_user_manipulations(request.user.id, unit.id)

				is_auth = request.user.is_authenticated()
				products_panel = Base().build_products_panel(is_auth)

				return render_to_string(unit.Template, {
					'unit': unit,
					'products_panel': products_panel,
					'is_auth': is_auth,
				})


			# level_2 и выше, если не лист дерева, и не товар на продажу
			if not self.is_leaf(unit) and not unit.UnitForPay:

				self.__write_user_manipulations(request.user.id, unit.id)

				# панель карусели
				unit_carousel_panel = Base().build_unit_carousel_panel(unit.id)

				units = Unit.objects.filter(parent_id=unit.id).values(
					'Name',
					'id',
					'Description',
					'unitphoto__Small',
					'level',
					'UnitForPay',
				)

				# для предпоследней ветки специальные html
				if units.exists():
					if units[0]['UnitForPay']==True:
						units_htmls = self.__build_unit_htmls_for_tree(
							units, 'tree_unit.html'
						)
					# для остальных
					else:
						units_htmls = self.__build_unit_htmls_for_tree(
							units,
							'tree_unit_level2.html',
						)

				return render_to_string('tree_level.html', {
					'parents_tree': parents_tree,
					'unit': unit,
					'units': units_htmls,
					'is_auth': self.__check_locked(parents_tree, unit, request),
					'locked_for_users': self.__locked_for_users(parents_tree, unit),
					'request': request,
					# для отключения / включения карусели
					'unit_carousel_panel': unit_carousel_panel,
				})


			# если товар на продажу
			if unit.UnitForPay:

				self.__write_user_manipulations(request.user.id, unit.id)

				properties = Unit.objects.filter(parent_id=unit.id).values('Name', 'id', 'Description', \
					'level')

				main_photo = UnitPhoto.objects.filter(Unit_id=unit.id)

				if main_photo.exists():
					main_photo = main_photo[0]
					photos_children = UnitPhoto.objects.filter(parent_id=main_photo.id)
				else:
					main_photo = False
					photos_children = []

				###########
				# связанные товары
				associated_parent = AssociatedParent.objects.filter(Unit_id=unit.id)
				if associated_parent.exists():

					associated_childrens = AssociatedChildren.objects.filter(
						AssociatedParent_id=associated_parent[0].id)
					if associated_childrens.exists():
						associated_units = list(Unit.objects.filter(id__in=[
							associated_children.Unit_id for associated_children in associated_childrens]
							).values(
								'id', 'Name', 'level', 'unitphoto__Small',
							))[:self.carousel_items_count]
						associated_parent_id = associated_parent[0].id
						type = associated_parent[0].type
				else:
					type = False
					associated_units = []
					associated_parent_id = 0
				carousel_items_panel = render_to_string('carousel_items_panel.html', {
					'type': type,
					'associated_units': associated_units,
					'associated_parent_id': associated_parent_id,
					'carousel_items_count': self.carousel_items_count,
				})

				#####
				## если видеокамеры в технических характеристиках таблица Особенности
				if any(u.id == 15 for u in parents_tree):
					camera_brunch = True
				else:
					camera_brunch = False

				json_unit = self.__build_json_unit(unit)

				return render_to_string('unit_for_pay_sts.html', {
					'main_photo': main_photo,
					'photos_children': photos_children,
					'parents_tree': parents_tree,
					'properties': properties,
					'unit': unit,
					'carousel_items_panel': carousel_items_panel,
					'is_auth': self.__check_locked(parents_tree, unit, request),
					'locked_for_users': self.__locked_for_users(parents_tree, unit),
					'camera_brunch': camera_brunch,
					'json_unit': json_unit,
				})


			# level_2 раздел сертификаты
			if unit.level == 1 and unit.id == 9:

				self.__write_user_manipulations(request.user.id, unit.id)

				return render_to_string(unit.Template, {
					'unit': unit,
				})

	# страница контакты
	def build_contacts_html(self):
		return render_to_string('page_interpreter/contacts_content.html')


	# построить дерево предков для priority_unit
	def build_priority_unit_parents_tree(self, priority_unit, exp=None):
		if exp is None:
			exp = []
		if priority_unit.parent_id:
			exp.append(priority_unit)
			return self.build_parents_tree(UnitPriority.objects.get(id=priority_unit.parent_id), exp)
		else:
			exp.append(priority_unit)
			exp = exp[::-1]
			return exp

	# построить дерево предков для unit
	def build_parents_tree(self, unit, exp=None):
		if exp is None:
			exp = []
		if unit.parent_id:
			exp.append(unit)
			return self.build_parents_tree(Unit.objects.get(id=unit.parent_id), exp)
		else:
			exp.append(unit)
			exp = exp[::-1]
			# показываем первый, предпоследний, последний элементы дерева
			export = []
			export.append(exp[0])
			if len(exp) > 3:
				exp[1].Name = '...'
				export.append(exp[1])
			export.append(exp[len(exp)-2])
			export.append(exp[len(exp)-1])
			return export

	# проверить unit последний ли в ветке дерева
	def is_leaf(self, unit):
		if Unit.objects.filter(parent_id=unit.id).exists():
			return False
		else:
			return True

	def build_priority_content_panel(self, priority_content, request):
		return self.__build_priority_content_html(priority_content, request)

	def build_content_panel(self, unit, request):
		return self.build_content_html(unit, request)

	def build_contacts_panel(self):
		return self.build_contacts_html()

	# построить панель поиска
	def build_search_panel(self, words):
		if words != '0':
			inputed_words = copy.deepcopy(urllib.parse.unquote(
				words))
			words = Ajax().remove_punctuation(urllib.parse.unquote(
				words)).split()

			unit_properties = Base().get_unit_properties()

			if len(words) > 1:
				units = Unit.objects.filter( \
					reduce(lambda x, y: x & y, [Q(Name__icontains=word) for word in words])).exclude(
					id__in=unit_properties,
				)
			elif len(words) == 1:
				units = Unit.objects.filter(Name__icontains=words[0]).exclude(id__in=unit_properties)
			elif len(words) == 0:
				units = False

			search_result_table = render_to_string('search_result_table.html', {
				'units': units,
			})
		else:
			search_result_table = ''
			inputed_words = ''
		return render_to_string('search_panel.html', {
			'search_result_table': search_result_table,
			'inputed_words': inputed_words,
		})

	def build_cart(self, request):
		args = {}

		if 'units' in request.session:

			for unit in request.session['units']:

				unit_object =  Unit.objects.get(id=unit['unit_id'])

				unit['name'] = unit_object.Name
				unit['level'] = unit_object.level

			units = request.session['units']
		else:
			units = None
		return render_to_string('cart/cart_page.html', {
			'units': units,
		})

	# сделать ветку стс скрытой
	def make_locked(self):
		self.sts_units_count = 0
		sts_branch = Unit.objects.filter(Name='специальные технические средства')
		return {
					'result': self.__lock_branch_by_id([child.id for child in sts_branch]),
					'updated_count': self.sts_units_count,
				}


	def __lock_branch_by_id(self, ids):
		children = Unit.objects.filter(parent_id__in=ids)
		sts_units = Unit.objects.filter(id__in=ids)
		sts_units.update(Locked=1)
		self.sts_units_count += len(sts_units)
		if len(children) > 0:
			self.__lock_branch_by_id([child.id for child in children])
		else:
			return True

	#  записать действие пользователя
	def __write_user_manipulations(self, user_id, unit_id):
		if user_id:
			UserManipulation.objects.create(User_id=user_id, Unit_id=unit_id)
