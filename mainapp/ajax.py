from mainapp.models import *
from django.template.loader import render_to_string
from mainapp.base import Base
from django.contrib import auth
from .mail import Mail
import urllib
import re

from functools import reduce
from django.db.models import Q

from .cart import Cart as Cart_module

class Ajax:

	def __init__(self):
		self.punctuations = re.compile('([-_<>?/\\".,{}@#!&()=+:;«»—*])')

	def remove_punctuation(self, string):
		string = self.punctuations.sub('', string)
		string = ' '.join(string.splitlines())
		return string


	def get_units_from_looped_list(self, units, index, result_list, result_list_length):
		if len(result_list) < result_list_length:
			if index > len(units)-1:
				index = 0
				result_list.append(units[index])
				return self.get_units_from_looped_list(units, index+1, result_list, result_list_length)
			else:
				result_list.append(units[index])
				return self.get_units_from_looped_list(units, index+1, result_list, result_list_length)

	def go_previous(self, unit_index, units, result_list, result_list_length):
		for index in range(0, len(units), 1):
			if unit_index == index:
				if index-1 < 0:
					index = len(units)-1
					result_list.append(units[index])
				else:
					index-=1
					result_list.append(units[index])
				self.get_units_from_looped_list(units, index+1, result_list, result_list_length)
				break
		return result_list

	def go_next(self, unit_index, units, result_list, result_list_length):
		for index in range(0, len(units), 1):
			if unit_index  == index:
				if index+1 >len(units)-1:
					index = 0
					result_list.append(units[index])
				else:
					index+=1
					result_list.append(units[index])
				self.get_units_from_looped_list(units, index+1, result_list, result_list_length)
				break
		return result_list

	def process_request(self, string, request):

		if string['method'] == 'get_carousel_units':

			associated_parent_id = string['associated_parent_id']

			first_unit_id = string['first_unit_id']

			carousel_action_type = string['type']

			items_count = string['items_count']

			associated_childrens = AssociatedChildren.objects.filter(
				AssociatedParent_id=associated_parent_id)

			associated_units = list(Unit.objects.filter(id__in=[
				associated_children.Unit_id for associated_children in associated_childrens]
				).values(
					'id', 'Name', 'level', 'unitphoto__Small'
				))

			result = []

			for index in range(0, len(associated_units), 1):
				if associated_units[index]['id']==first_unit_id:
					if carousel_action_type == 'previous':
						self.go_previous(
							index,
							associated_units,
							result,
							items_count,
						)
					elif carousel_action_type == 'next':
						self.go_next(
							index,
							associated_units,
							result,
							items_count,
						)
			return {
				'html': render_to_string('carousel_items_panel.html', {
					'associated_units': result,
					'associated_parent_id': associated_parent_id,
					'carousel_items_count': items_count,
				}),
			}

		if string['method'] ==  'quantity_change':
			quantity = string['quantity']

			unit_id = string['unit_id']

			result = Cart_module().quantity_change(unit_id, quantity, request)

			return {
				'session_units': result['session_units'],
				'cart_html': result['cart_html'],
			}

		# добавить в корзину
		if string['method'] == 'add_to_cart':

			quantity = string['quantity']

			unit_id = string['unit_id']

			result = Cart_module().add_to_cart(unit_id, quantity, request)

			return {
				'session_units': result['session_units'],
				'cart_html': result['cart_html'],
			}

		# выйти
		if string['method'] == 'logout':

			auth.logout(request)

			login_panel_html = Base().build_login_panel(request)

			return {
				'login_panel_html': login_panel_html,
			}

		# если отправить заказ с корзиной
		if string['method'] == 'submit_cart':
			
			name = string['name']
			telephone = string['telephone']
			email = string['email']
			text = string['text']

			for unit in request.session['units']:
				unit['name'] = Unit.objects.get(id=unit['unit_id'])

			if len(request.session['units'])>0:
				products_line = []
				for unit in request.session['units']:
					products_line.append(
						'Наименование:{0}. Количество:{1}.'.format(unit['name'], unit['quantity']))

				products_line = ' '.join(products_line)

				Cart.objects.create(
					Name=name,
					Telephone=telephone,
					Email=email,
					Text=text,
					Units=products_line
				)

				subj = 'Заказ на сайте Новые технологии'
				text = 'Имя:{0}.Телефон:{1}.Э/п:{2}.Текст:{3}.Товары. {4}'.format(
					name,
					telephone,
					email,
					text,
					products_line
				)
				mail = Mail().fill(subj, text)
				mail.send()

				request.session['units'] = []
				return {
					'status': 'success',
					'answer': 'sucessfuly added to order',
					'cart_html': Cart_module().build_cart_html(request),
				}
			else:
				return {
					'status': 'error',
					'answer': 'cart_is_empty',
					'cart_html': Cart_module().build_cart_html(request),
				}

		if string['method'] == 'submit_letter':

			letter_name = string['letter_name']
			letter_telephone = string['letter_telephone']
			letter_email = string['letter_email']
			letter_text = string['letter_text']

			ContactsLetter.objects.create(
				Name = letter_name,
				Telephone = letter_telephone,
				Email = letter_email,
				Text = letter_text,
 			)
			subj = 'Сообщение на сайте Новые технологии'
			text = 'Имя:{0}.\nТелефон:{1}.\nEmail:{2}.\nТекст:{3}.'.format( \
				letter_name, letter_telephone, letter_email, letter_text)
			mail = Mail().fill(subj, text)
			mail.send()

			return {
				'email_result': 'ok',
			}

		# результаты поиска
		if string['method'] == 'process_searching':


			words = self.remove_punctuation(urllib.parse.unquote(
				string['words'])).split()

			unit_properties = Base().get_unit_properties()

			if len(words) > 1:
				units = Unit.objects.filter( \
					reduce(lambda x, y: x & y, [Q(Name__icontains=word) for word in words])).exclude(
					id__in=unit_properties,
				)
			elif len(words) == 1:
				units = Unit.objects.filter(Name__icontains=words[0]).exclude(id__in=unit_properties)
			elif len(words) == 0:
				units = False

			table = render_to_string('search_result_table.html', {
				'units': units,
			})

			return {
				'answer': table,
			}

		# просмотр фотографии
		if string['method'] == 'view_photo':

			photo_id = string['photo_id']
			photo = UnitPhoto.objects.get(id=photo_id)

			unit_photo_tree = Base().build_unit_photo_tree(photo_id)

			next_element = Base().get_next_of_unit_photo_tree(unit_photo_tree)

			previous_element = Base().get_previous_of_unit_photo_tree(unit_photo_tree)

			if len(unit_photo_tree) > 1:
				can_switch = True
			else:
				can_switch = False

			photo_html = render_to_string('view_photo.html', {
				'photo': photo,
				'next_element': next_element,
				'previous_element': previous_element,
				'can_switch': can_switch,
			})

			return {
				'answer': photo_html
			}			

		# отправить форму заявки на авторизацию
		if string['method'] == 'submit_registration':

			name = string['name']
			email = string['email']
			telephone = string['telephone']
			organization = string['organization']
			text = string['text']

			RegistrationOrder.objects.create(
				Name = name,
				Email = email,
				Telephone = telephone,
				Organization = organization,
				Text = text
			)

			subj = 'Заявка на регистрацию на сайте Новые технологии'
			text = 'Имя:{0},\nЭлектр.почта:{1},\nТелефон:{2},\nОрганизация:{3},\nТекст:{4}.'.format( \
				name, email, telephone, organization, text
				)
			mail = Mail().fill(subj, text)
			mail.send()

			return {
				'status': 'success',
				'text': 'Заявка на авторизацию успешно создана',
			}

		# сформировать форму регистрации
		if string['method'] == 'build_registration_form':

			registration_form = render_to_string('registration_form.html')

			return {
				'registration_form': registration_form,
			}

		# результат авторизации
		if string['method'] == 'try_login':

			login = string['login']

			password = string['password']

			user = auth.authenticate(username=login, password=password)

			if not user is None:
				auth.login(request, user)
				return {
					'answer': True,
				}
			else:
				return {
					'answer': False,
				}
			

		# сформировать форму авторизации
		if string['method'] == 'build_auth_form':

			auth_form = render_to_string('auth_form.html', {'url': string['url']})

			return {
				'auth_form': auth_form
			}

		if string['method'] == 'get_subheader_units':

			## подразделы
			subheader_units = Unit.objects.filter(parent_id=string['unit_id']).values( \
				'id', 'Name')

			subheader_units_html = []

			for subheader_unit in subheader_units:
				subheader_units_html.append(render_to_string('subheader_unit.html', {
						'name': subheader_unit['Name'],
						'id': subheader_unit['id']
					}))	
			## фото для подразделов
			s_us = []
			for i in subheader_units:
				s_us.append(i['id'])

			subheader_photoes = Photo.objects.filter(Unit_id__in=s_us, \
				Large='').exclude(Small='').values( \
				'Unit_id', 'Unit__Name', 'Small')

			subheader_photoes_html = []
			for subheader_photo in subheader_photoes:
				subheader_photoes_html.append(render_to_string('subheader_photo.html', {
					'name': subheader_photo['Unit__Name'],
					'unit_id': subheader_photo['Unit_id'],
					'img': subheader_photo['Small']
					}))

			## сопутствующие товары
			subheader_children = Unit.objects.filter(parent_id__in=s_us).values('id', 'Name', 'parent_id')

			subheader_children_html = []
			for subheader_child in subheader_children:
				subheader_children_html.append(render_to_string('subheader_child.html', {
						'name': subheader_child['Name'],
						'id': subheader_child['id'],
						'parent_id': subheader_child['parent_id'],
					}))
			## фото для сопутствующих товаров
			sc_us = []
			for i in subheader_children:
				sc_us.append(i['id'])

			subheader_children_photoes = Photo.objects.filter(Unit_id__in=sc_us, \
				Large='').exclude(Small='').values( \
				'Unit_id', 'Unit__Name', 'Small')

			subheader_children_photoes_html = []

			for subheader_children_photo in subheader_children_photoes:
				subheader_children_photoes_html.append(render_to_string('subheader_photo.html', {
					'name': subheader_children_photo['Unit__Name'],
					'unit_id': subheader_children_photo['Unit_id'],
					'img': subheader_children_photo['Small']
					}))

			return {
				'subheader_units_html': subheader_units_html,
				'subheader_photoes_html': subheader_photoes_html,
				'subheader_children_html': subheader_children_html,
				'subheader_children_photoes_html': subheader_children_photoes_html
			}
		if string['method'] == 'submit_form':
			location = string['location']
			organization = string['organization']
			production = string['production']
			name = string['name']
			telephone = string['telephone']
			email = string['email']
			text = string['text']

			RegistrationLetter.objects.create(
				Location=location,
				Organization=organization,
				Production=production,
				Name=name,
				Telephone=telephone,
				Email=email,
				Text=text
			)

			subj = 'Заявка на сайте Новые технологии'
			text = \
'Нас.пункт:{4}.\nОрганизация:{5}.\nПродукция:{6}.\nИмя:{0}.\nТелефон:{1}.\nEmail:{2}.\nТекст:{3}.'.format( \
				name, telephone, email, text, location, organization, production)
			mail = Base().fill(subj, text)
			mail.send()
			return {
				'email_result': 'ok',
			}