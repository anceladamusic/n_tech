import smtplib
from email.mime.text import MIMEText
import sys
import traceback
import os
import operator

from django.template import *
from .models import *
from django.template.loader import render_to_string
from django.http import HttpResponse
from django.contrib import auth
from django.db.models import Q

class Base:

	def __init__(self):
		self.path = os.path.abspath(os.path.dirname(__file__))
		self.carousel_panel = self.__build_carousel_panel()

	def build_login_panel(self, request):
		user = auth.get_user(request)
		if user.id != None:
			username = user.username
		else:
			username = None
		return render_to_string('login_panel.html', {
			'username': username,
			'request': request,
		})

	def build_products_panel(self, is_auth):

		productions = Unit.objects.filter(parent_id=2)

		return render_to_string('products_panel.html', {
			'productions': productions,
			'is_auth': is_auth,
		})

	# карусель в разделе дерева
	def build_unit_carousel_panel(self, unit_id):

		unit_carousel_objects = UnitCarouselChildren.objects.filter(
			UnitCarouselParent__Unit_id=unit_id).values(
			'Unit_id',
			'Unit__level',
			'Title',
			'Description',
			'Image',
		)

		print (unit_carousel_objects)

		if len(unit_carousel_objects)>0:
			unit_carousel_panel = render_to_string('unit_carousel_panel.html', {
				'unit_carousel_objects': unit_carousel_objects,
			})
			return unit_carousel_panel
		else:
			return None

	# карусель
	def __build_carousel_panel(self):

		priority_units = UnitPriority.objects.filter(level=0)

		carousel_panel = render_to_string('carousel_panel.html', {
				'priority_units': priority_units,
			})
		return carousel_panel

	def __get_header_center_units(self, level_1, unit=None):
		header_units = Unit.objects.filter(level=1)
		header_unit_htmls = []
		for header_unit in header_units:
			header_unit_htmls.append( \
				render_to_string('header_paragraph_unit.html', {
					'name': header_unit.Name, \
				 	'id': header_unit.id,
				 	'level_1': level_1,
				 	'selected_unit_id': unit.id,
			 	}))
		return header_unit_htmls

	def __get_parent_level_of_unit(self, level, unit):
		if unit.level > level:
			pretendent = Unit.objects.get(id=unit.parent_id)
			if pretendent.level > level:
				return self.__get_parent_level_of_unit(level, pretendent)
			else:
				return pretendent
		else:
			return unit

	def build_header_panel(self, unit):
		level_1 = self.__get_parent_level_of_unit(1, unit)
		header_unit_htmls = self.__get_header_center_units(level_1, unit)
		return render_to_string('header_paragraph_panel.html', {
			'header_unit_htmls': header_unit_htmls
		})

	def download_catalogue(self):
		path = '{0}/test.xls'.format(self.path)

		if os.path.exists(path):
			with open(path, 'rb') as xls_file:
				response = HttpResponse(xls_file.read(), content_type='application/vnd.ms-excel')

				response['Content-Disposition'] = 'inline; filename="test.xls"'

				return response
		else:
			return HttpResponse('file not found')

	def build_unit_photo_tree(self, photo_id):

		unit_photo = UnitPhoto.objects.get(id=photo_id)

		if unit_photo.parent_id == None:

			else_photoes = UnitPhoto.objects.filter(parent_id=unit_photo.id)

		else:

			else_photoes = UnitPhoto.objects.filter(
				Q(parent_id=unit_photo.parent_id)|Q(id=unit_photo.parent_id)
			).exclude(id=unit_photo.id)

		photoes_tree = []

		photoes_tree.append({'unit': unit_photo, 'selected': 1, 'id': unit_photo.id})

		for else_photo in else_photoes:
			photoes_tree.append({'unit': else_photo, 'selected': 0, 'id': else_photo.id})

		photoes_tree = self.sort_list_of_dict_by_key(photoes_tree, 'id')

		return photoes_tree

	def sort_list_of_dict_by_key(self, list_of_dict, key):
		list_of_dict.sort(key=operator.itemgetter(key))
		return list_of_dict

	def get_next_of_unit_photo_tree(self, unit_photo_tree):

		last_elem = unit_photo_tree[len(unit_photo_tree)-1]
		first_elem = unit_photo_tree[0]

		for key, unit_photo in enumerate(unit_photo_tree):
			if unit_photo['selected'] == 1:
				if unit_photo == last_elem:
					return first_elem
				else:
					return unit_photo_tree[key+1]


	def get_previous_of_unit_photo_tree(self, unit_photo_tree):

		last_elem = unit_photo_tree[len(unit_photo_tree)-1]
		first_elem = unit_photo_tree[0]

		for key, unit_photo in enumerate(unit_photo_tree):
			if unit_photo['selected'] == 1:
				if unit_photo == first_elem:
					return last_elem
				else:
					return unit_photo_tree[key-1]

	def get_unit_properties(self):

		units_for_pay = Unit.objects.filter(UnitForPay=1)

		unit_properties = Unit.objects.filter(parent_id__in=units_for_pay)

		return unit_properties
