from django.conf.urls import include, url
from mainapp import views as mainapp_views
from django.conf.urls.static import static

urlpatterns = [

	url(r'^$', mainapp_views.main_page),

	# url(r'^level/(?P<level>[0-9]+)/unit/(?P<unit>[0-9]+)/$', mainapp_views.unit),

	# url(r'^level/(?P<level>[0-9]+)/unit_priority/(?P<unit_priority>[0-9]+)/$', \
	#  mainapp_views.unit_priority),

	# url(r'^ajax/$', mainapp_views.ajax),

	# url(r'^download_catalogue/$', mainapp_views.download_catalogue),

	# url(r'^contacts/$', mainapp_views.contacts),

	# url(r'^search/(?P<words>.*)/$', mainapp_views.search),

	# url(r'^cart/$', mainapp_views.cart),

	# url(r'^cart_remove/unit/(?P<unit>[0-9]+)/$', mainapp_views.cart_remove),

	# # url(r'^make_locked/', mainapp_views.make_locked),

	# url(r'^pdf_view/(?P<file>.*)/$', mainapp_views.pdf_view),

]
