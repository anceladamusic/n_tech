import mptt
from mptt.models import MPTTModel, TreeForeignKey
from mptt import managers
from mptt.managers import TreeManager
from django.utils import timezone
from django.db import models
from django.contrib.auth.models import User


# Товары, разделы, страницы
class Unit(MPTTModel):

    class Meta:
        db_table = 'Unit'

    class MPTTMeta:
        order_insertion_by = ['Name']

    Name = models.CharField(max_length=200, blank=True, null=True)
    Description = models.TextField(blank=True, null=True)
    Description2 = models.TextField(blank=True, null=True)
    TechnicalCharacteristic = models.TextField(blank=True, null=True)

    Functional = models.TextField(blank=True, null=True)
    Features = models.TextField(blank=True, null=True)

    ShortInf = models.TextField(blank=True, null=True)
    Locked = models.BooleanField(default=False)
    LockedForUser = models.BooleanField(
        'Заблокировать для пользователей', default=False)
    Ico = models.ImageField(upload_to='media/', blank=True, null=True)
    Background = models.ImageField(
        upload_to='media/background/', blank=True, null=True)
    Template = models.FileField(upload_to='templates/', blank=True, null=True)
    UnitForPay = models.BooleanField(default=False)
    parent = TreeForeignKey('self', blank=True, null=True,
                            related_name='children', db_index=True)

    tree = TreeManager()

    def __str__(self):
        return self.Name


mptt.register(Unit, order_insertion_by=['Name'])

# Связанные товары: Родитель


class AssociatedParent(models.Model):
    class Meta:
        db_table = 'AssociatedParent'
    Unit = models.ForeignKey(Unit, on_delete=models.CASCADE, null=True)
    type = models.BooleanField(verbose_name='связанный товар?', default=False)

    def __str__(self):
        return '{0}'.format(self.Unit)

# Связанные товары: Дочерний


class AssociatedChildren(models.Model):
    class Meta:
        db_table = 'AssociatedChildren'
    AssociatedParent = models.ForeignKey(
        AssociatedParent, on_delete=models.CASCADE, null=True)
    Unit = models.ForeignKey(Unit, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return 'Родитель:{0}| Дочерний:{0}'.format(self.AssociatedParent, self.Unit)

################################
# Основные комплексы на продажу


class UnitPriority(MPTTModel):

    class Meta():
        db_table = 'UnitPriority'

    class MPTTMeta:
        order_insertion_by = ['Name']

    Name = models.CharField(max_length=200, blank=True, null=True)
    Description = models.TextField(blank=True, null=True)
    ShortInf = models.TextField(blank=True, null=True)
    Locked = models.BooleanField(default=False)
    Ico = models.ImageField(upload_to='media/ico/', blank=True, null=True)
    Image = models.ImageField(upload_to='media/', blank=True, null=True)
    Schema = models.ImageField(
        upload_to='media/schema/', blank=True, null=True)
    Template = models.FileField(upload_to='templates/', blank=True, null=True)
    UnitForPay = models.BooleanField(default=False)
    parent = TreeForeignKey('self', blank=True, null=True,
                            related_name='children', db_index=True)
    Unit = TreeForeignKey(Unit, null=True, on_delete=models.CASCADE)

    tree = TreeManager()

    def __str__(self):
        return self.Name

# фотографии


class UnitPhoto(MPTTModel):

    class Meta():
        db_table = 'UnitPhoto'

    class MPTTMeta:
        db_table = 'UnitPhoto'
    Description = models.TextField(default='Нет описания', blank=True)
    Large = models.ImageField(upload_to='media/', blank=True, null=True)
    Small = models.ImageField(upload_to='media/', blank=True, null=True)
    parent = TreeForeignKey('self', blank=True, null=True,
                            related_name='children', db_index=True)
    Unit = models.ForeignKey(Unit, on_delete=models.CASCADE, null=True)
    UnitPriority = models.ForeignKey(
        UnitPriority, on_delete=models.CASCADE, null=True)

    tree = TreeManager()

    def __str__(self):
        return '{0} |Unit_for_pay:{4} |{1} | МалоеФото:{2} | Большое фото:{3}'.format(
            self.Unit, self.Description[:15], self.Small, self.Large, self.UnitPriority)


mptt.register(UnitPhoto)

# письма - заявки на регистрацию


class RegistrationLetter(models.Model):
    class Meta():
        db_table = 'RegistrationLetter'
    Production = models.CharField(max_length=512, default='Не указано')
    Location = models.CharField(max_length=512, default='Не указано')
    Organization = models.CharField(max_length=512, default='Не указано')
    Name = models.CharField(max_length=256)
    Telephone = models.CharField(max_length=256)
    Email = models.CharField(max_length=256)
    Text = models.TextField()
    Date = models.DateTimeField(default=timezone.now)

# запись ошибок сайта


class SiteError(models.Model):
    class Meta():
        db_table = 'SiteError'
    Text = models.TextField()
    Date = models.DateTimeField(default=timezone.now)

############################
# заявки на регистрацию


class RegistrationOrder(models.Model):
    class Meta():
        db_table = 'RegistrationOrder'

    Name = models.CharField(max_length=256)
    Email = models.CharField(max_length=256)
    Telephone = models.CharField(max_length=20, default='Не указан')
    Organization = models.CharField(max_length=256)
    Text = models.TextField(default='Нет текста')
    Date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return 'Имя:{0}|Организация:{1}|Телефон:{2}'.format(self.Name, self.Organization, self.Telephone)

###############################
# Письма на страниче Контактов


class ContactsLetter(models.Model):
    class Meta():
        db_table = 'ContactsLetter'

    Name = models.CharField(max_length=256)
    Email = models.CharField(max_length=250)
    Telephone = models.CharField(max_length=20)
    Email = models.CharField(max_length=256, default='Не указан')
    Text = models.TextField()
    Date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return 'Имя:{0}|Телефон:{1}'.format(Name, Telephone)


class Cart(models.Model):
    class Meta():
        db_table = 'Cart'

    Name = models.CharField(max_length=256)
    Telephone = models.CharField(max_length=20)
    Email = models.CharField(max_length=256, default='Не указан')
    Text = models.TextField()
    Date = models.DateTimeField(default=timezone.now)
    Units = models.TextField()

##############################
# Карусель на разделах дерева


class UnitCarouselParent(models.Model):

    class Meta():
        db_table = 'UnitCarouselParent'

    Unit = models.ForeignKey(Unit, on_delete=models.CASCADE)

    def __str__(self):
        return '{0}'.format(self.Unit)


class UnitCarouselChildren(models.Model):

    class Meta():
        db_table = 'UnitCarouselChildren'

    UnitCarouselParent = models.ForeignKey(
        UnitCarouselParent, on_delete=models.CASCADE)
    Unit = models.ForeignKey(Unit, on_delete=models.CASCADE)
    Title = models.CharField(max_length=200, blank=True, null=True)
    Description = models.TextField(blank=True, null=True)
    Image = models.ImageField(
        upload_to='media/unit_carousel/', blank=True, null=True)

    def __str__(self):
        return '{0}'.format(self.Unit)

##############################
# запись манипуляций пользователя


class UserManipulation(models.Model):

    class Meta():
        db_table = 'UserManipulation'

    User = models.ForeignKey(User, on_delete=models.CASCADE)
    Unit = models.ForeignKey(Unit, on_delete=models.CASCADE)
    Date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return 'пользователь: {0}  |   страница: {1}  |  {2}'.format(self.User, self.Unit, self.Date)
