import os
from django.conf import settings
import json

from django.template.loader import render_to_string
from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse, Http404
from django.template import RequestContext

from .models import *
from .cart import Cart
from .base import Base
from .ajax import Ajax
from .page_interpreter import PageInterpreter
from meta.meta import Meta_tags
from structured.structured import Structured


def unit_priority(request, level=None, unit_priority=None):

    args = {}

    args['cart_html'] = Cart().build_cart_html(request)

    try:
        unit_priority = UnitPriority.objects.get(id=unit_priority)
    except UnitPriority.DoesNotExist:
        raise Http404
    args['header_panel'] = Base().build_header_panel(unit_priority)

    args['content'] = PageInterpreter().build_priority_content_panel(
        unit_priority, request)

    args['login_panel'] = Base().build_login_panel(request)

    return render(request, 'index.html', args)


def unit(request, level=None, unit=None):

    args = {}

    args['cart_html'] = Cart().build_cart_html(request)

    try:
        unit = Unit.objects.get(id=unit, level=level)
    except Unit.DoesNotExist:
        raise Http404

    # meta
    args['meta'] = Meta_tags().build_meta_tags(unit)

    args['header_panel'] = Base().build_header_panel(unit)

    args['content'] = PageInterpreter().build_content_panel(unit, request)

    args['login_panel'] = Base().build_login_panel(request)

    return render(request, 'index.html', args, RequestContext(request))


def ajax(request):
    if request.method == 'POST':
        response_body = Ajax().process_request(json.loads(request.body), request)
        return JsonResponse(response_body)


def main_page(request):
    return HttpResponse('Сайт на реконструкции')
    args = {}

    # meta
    args['meta'] = Meta_tags().build_meta_tags(1)

    # json_ld
    args['json_ld'] = Structured().build_structured(1)

    args['cart_html'] = Cart().build_cart_html(request)

    args['header_panel'] = Base().build_header_panel(Unit.objects.get(id=1))

    args['carousel'] = Base().carousel_panel

    args['content'] = PageInterpreter().build_main(request)

    args['login_panel'] = Base().build_login_panel(request)

    return render(request, 'index.html', args)


def download_catalogue(request):
    return Base().download_catalogue()


def contacts(request):
    args = {}

    args['cart_html'] = Cart().build_cart_html(request)

    # meta
    args['meta'] = Meta_tags().build_meta_tags(10)

    args['header_panel'] = Base().build_header_panel(Unit.objects.get(id=10))

    args['content'] = PageInterpreter().build_contacts(request)

    args['login_panel'] = Base().build_login_panel(request)

    return render(request, 'index.html', args)


def search(request, words=None):
    args = {}

    args['cart_html'] = Cart().build_cart_html(request)

    # meta
    args['meta'] = Meta_tags().build_meta_tags(164)

    args['header_panel'] = Base().build_header_panel(Unit.objects.get(id=164))

    args['content'] = PageInterpreter().build_search_panel(words)

    args['login_panel'] = Base().build_login_panel(request)

    return render(request, 'index.html', args)


def cart(request):
    args = {}

    args['cart_html'] = Cart().build_cart_html(request)

    args['header_panel'] = Base().build_header_panel(Unit.objects.get(id=1))

    args['content'] = PageInterpreter().build_cart(request)

    args['login_panel'] = Base().build_login_panel(request)

    return render(request, 'index.html', args)


def cart_remove(request, unit=None):
    Cart().cart_remove(unit, request)
    return redirect('/cart/')


def make_locked(request):
    result = PageInterpreter().make_locked()
    return JsonResponse(result)


def pdf_view(request, file=None):

    path = '{0}/media/pdf/{1}'.format(settings.BASE_DIR, file)

    if os.path.isfile(path):

        with open(path, 'rb') as pdf:

            response = HttpResponse(pdf.read(), content_type='application/pdf')

            response['Content-Disposition'] = 'inline;filename={0}'.format(
                file)

            return response

    else:

        raise Http404
