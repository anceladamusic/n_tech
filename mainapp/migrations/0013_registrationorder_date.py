# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-20 07:17
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0012_registrationorder'),
    ]

    operations = [
        migrations.AddField(
            model_name='registrationorder',
            name='Date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
