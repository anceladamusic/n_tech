# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-05-23 10:59
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0039_auto_20170503_0905'),
    ]

    operations = [
        migrations.CreateModel(
            name='UnitCarousel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Name', models.CharField(blank=True, max_length=200, null=True)),
                ('Description', models.TextField()),
                ('Image', models.FileField(blank=True, null=True, upload_to='media/unit_carousel/')),
                ('Unit', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mainapp.Unit')),
            ],
            options={
                'db_table': 'UnitCarousel',
            },
        ),
    ]
