# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-20 18:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0013_registrationorder_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='unit',
            name='UnitForPay',
            field=models.BooleanField(default=False),
        ),
    ]
