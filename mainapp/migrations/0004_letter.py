# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-28 21:35
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0003_unit_background'),
    ]

    operations = [
        migrations.CreateModel(
            name='Letter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Name', models.CharField(max_length=256)),
                ('Telephone', models.CharField(max_length=256)),
                ('Email', models.CharField(max_length=256)),
                ('Date', models.DateTimeField(default=django.utils.timezone.now)),
            ],
            options={
                'db_table': 'Letter',
            },
        ),
    ]
