# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-04-04 10:06
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0032_auto_20170404_1251'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='cart',
            table='Cart',
        ),
    ]
