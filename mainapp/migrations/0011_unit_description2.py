# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-17 05:19
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0010_unit_locked'),
    ]

    operations = [
        migrations.AddField(
            model_name='unit',
            name='Description2',
            field=models.TextField(blank=True, null=True),
        ),
    ]
