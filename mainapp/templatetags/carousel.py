from django import template

register = template.Library()


@register.filter
def next(carousel_list, index):
	if index+1 < len(carousel_list):
		if len(carousel_list) > 0:
			return carousel_list[index+1].id
	else:
		if len(carousel_list) > 0:
			return carousel_list[0].id

@register.filter
def previous(carousel_list, index):
	if index-1 > -1:
		if len(carousel_list) > 0:
			return carousel_list[index-1].id
	else:
		if len(carousel_list) > 0:
			return carousel_list[len(carousel_list)-1].id