import json
from django import template

register = template.Library()


@register.filter
def ascii_ignore(str):
	return str.encode('ascii', 'ignore')