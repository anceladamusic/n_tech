from django.template.loader import render_to_string
from django.contrib import auth
from .models import *


class Cart():

	def __init__(self):
		self.item = {
			'item': 0,
			'quantity': 0,
		}

	def cart_init_if_none(self, request):
		if 'cart' not in request.session:

			request.session['cart'] = {}
			request.session['cart']['units'] = []
			
			if auth.get_user(request).id == None:
				request.session['cart']['user_id'] = 'Anonimous'
			else:
				request.session['cart']['user_id'] = auth.get_user(request).id
		else:
			if auth.get_user(request).id == None:
				request.session['cart']['user_id'] = 'Anonimous'
			else:
				request.session['cart']['user_id'] = auth.get_user(request).id

	def build_cart_html(self, request):
		if 'units' in request.session:

			units_count = 0
			for unit in request.session['units']:
				units_count+=unit['quantity']
		else:
			units_count = 0

		cart_html = render_to_string('cart/cart.html', {
			'units_count': units_count,	
		})
		return cart_html


	def add_to_cart(self, unit, quantity, request):
		unit = Unit.objects.get(id=unit)

		if not 'units' in request.session:
			request.session['units'] = []

		request.session['units'].append({
			'unit_id': unit.id,
			'quantity': quantity,
		})

		return {
			'session_units': request.session['units'],
			'cart_html': self.build_cart_html(request),
		}

	def quantity_change(self, unit_id, quantity, request):
		for unit in request.session['units']:
			if unit['unit_id'] == unit_id:
				unit['quantity'] = quantity

		return {
			'session_units': request.session['units'],
			'cart_html': self.build_cart_html(request),
		}

	def cart_remove(self, unit_id, request):
		for key, unit in enumerate(request.session['units']):
			if unit['unit_id'] == int(unit_id):
				request.session['units'].remove(request.session['units'][key])