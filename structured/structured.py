import json

class Structured():
	def __init__(self):
		self.pages_ids = {
			1: {
				'jsons': json.dumps([
					self.build_contact(),
				], ensure_ascii=False)
			},
		}


	def build_structured(self, page):
		return self.pages_ids[page]

	def build_contact(self):
		contact = {
			"@context": "http://schema.org",
			"@type": "Organization",
			"url": "http://kb-nt.com",
			"address": {
				"@type": "PostalAddress",
				"addressLocality": "Казань, Россия",
				"postalCode": "420073",
				"streetAddress": "ул. Гвардейская 33, оф. 303-305"
			},
			"email": "info@kb-nt.com",
			"telephone": "+7-843-228-5307",
			"name": "СКБ Новые технологии",
			"telephone": "+7-843-228-5307"
		}
		return contact