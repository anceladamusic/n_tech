from django.apps import AppConfig


class StructuredConfig(AppConfig):
    name = 'structured'
