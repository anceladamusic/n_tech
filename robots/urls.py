from django.conf.urls import include, url
from robots import views

urlpatterns = [
	url(r'^robots.txt$', views.robots),
	url(r'^sitemap.xml$', views.sitemap),
]