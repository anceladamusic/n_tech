from django.shortcuts import render
from mainapp.models import *

# Create your views here.
def robots(request):
	units = Unit.objects.filter(Locked=True)
	return render(request, 'robots.txt', {
		'units': units,		
	},
	content_type="text/plain")

def sitemap(request):

	not_allowed_ids = [
		1,
		10,
		164,
	]

	units_for_pay = Unit.objects.filter(UnitForPay=True)

	units_properties = Unit.objects.filter(parent_id__in=units_for_pay)

	units = Unit.objects.exclude(
		id__in=units_properties
	).exclude(
		Locked=True
	).exclude(
		id__in=not_allowed_ids
	)

	return render(request, 'sitemap.xml', {
		'units': units,
	},
	content_type="text/plain")