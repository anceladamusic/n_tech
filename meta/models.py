from django.db import models
from mainapp.models import *

class Meta(models.Model):
	Title = models.CharField(max_length=200, null=True, blank=True)
	Description = models.TextField()
	Unit = models.ForeignKey(Unit, on_delete=models.CASCADE)

	def __str__(self):
		return '{0}'.format(self.Unit)